Let's apply *Git flow*


Features
1.  Develop new features for upcoming releases
2.  Typically exist in developers repos only

Start a new feature
Development of new features starting from the 'develop' branch.

Start developing a new feature with command "git checkout -b feature/<username>_<storycode>_<shortworkdesc>"

This action creates a new feature branch based on 'develop' and switches to it

Finish up a feature
Finish the development of a feature. This action performs the following

Merged MYFEATURE into 'develop'
Removes the feature branch
Switches back to 'develop' branch

This action is requiring several steps.
1. Add all changes by typing command "git add <filename>"
2. Reverting all unnecessary changes by checkout from its existing condition. This can be done by running "git checkout <reverting_filename>" 
3. Committing changes to your local machine's repository using "git commit -m "feature/<username>_<storycode>_<desc>" to let others know what things you are doing
4. git push
5. git checkout <develop_branch>
6. git push origin --delete feature/<username>_<storycode>_<shortworkdesc> <-- to remove your work branch to save spaces!

for a hotfix/bugfix change feature to bugfix

for a merge action change feature to merge

All is well

links:
http://danielkummer.github.io/git-flow-cheatsheet/#setup